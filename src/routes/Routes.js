import React from 'react';
import {Router,IndexRoute, Route, browserHistory} from 'react-router';
import {IndexPage, LoginPage, RegisterPage, ResetPasswordPage, VerifyEmailPage, ProfilePage} from '../pages';
import {DefaultLayout} from '../layouts'


export default class Routes extends React.Component {
    render() {
        return (
            <Router history={browserHistory}>
                <Route path='/' component={DefaultLayout}>
                    <IndexRoute component={IndexPage}/>
                    <Route path='/login' component={LoginPage}/>
                    <Route path='/verify' component={VerifyEmailPage}/>
                    <Route path='/register' component={RegisterPage}/>
                    <Route path='/forgot' component={ResetPasswordPage}/>
                    <Route path='/profile' component={ProfilePage}/>
                </Route>
            </Router>
        );
    }
}