import { Link } from 'react-router';
import React, { PropTypes } from 'react';
import DocumentTitle from 'react-document-title';

import {Header} from '../common';

export default class DefaultLayout extends React.Component {
    render() {
        return (
            <DocumentTitle title='My App'>
                <div className='MasterPage'>
                    <Header />
                    { this.props.children }
                </div>
            </DocumentTitle>
        );
    }
}